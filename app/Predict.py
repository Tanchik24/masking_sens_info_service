from typing import List

import mlflow
import numpy as np
import torch
from config import Config
from prepare_data_for_predict import prepare_data_for_predict
from tqdm import tqdm
from transformers import BertTokenizer
from utils import (accumulate_entity_predictions, download_artifacts,
                   get_device, get_entities, make_dataloader)


class Predictor:
    def __init__(
        self,
    ):
        model_name = "ner"
        model_tag = "Production"
        model_uri = f"models:/{model_name}/{model_tag}"
        self.model = mlflow.pytorch.load_model(model_uri)
        tokenizer_local_path, entities_local_path = download_artifacts()
        self.tokenizer = BertTokenizer.from_pretrained(tokenizer_local_path)
        self.device = get_device()
        self.model.to(self.device)
        self.entities = get_entities(entities_local_path)
        self.pad_token_label_id = Config.PAD_LABEL_ID
        self.model.eval()

    def batch_step(self, batch):
        batch = tuple(t.to(self.device) for t in batch)
        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "token_type_ids": batch[2],
            }

            return self.model(**inputs)

    def _process_predictions(
        self, entity_preds: np.ndarray, all_entity_label_mask: np.ndarray
    ) -> List[List[str]]:
        entity_preds = np.argmax(entity_preds, axis=2)
        entity_labels = self._decode_slot_predictions(
            entity_preds, all_entity_label_mask
        )

        return entity_labels

    def _decode_slot_predictions(
        self, entity_preds: np.ndarray, all_entity_label_mask: np.ndarray
    ) -> List[List[str]]:
        slot_label_map = {i: label for i, label in enumerate(self.entities)}
        slot_preds_list: List[List[str]] = [[] for _ in range(entity_preds.shape[0])]

        for i in range(entity_preds.shape[0]):
            for j in range(entity_preds.shape[1]):
                if all_entity_label_mask[i, j] != self.pad_token_label_id:
                    slot_preds_list[i].append(slot_label_map[entity_preds[i][j]])

        return slot_preds_list

    def predict(self, sentences: List[str]):
        dataset = prepare_data_for_predict(sentences, self.tokenizer)
        dataloader = make_dataloader(dataset, "test")
        entity_preds: np.ndarray = np.empty((0, 0))
        all_entity_label_mask: np.ndarray = np.empty((0, 0))
        for batch in tqdm(dataloader, desc="Predicting"):
            output = self.batch_step(batch)
            entity_preds, all_entity_label_mask = accumulate_entity_predictions(
                output, batch, entity_preds, all_entity_label_mask
            )

        entity_preds = (
            np.array(entity_preds) if entity_preds is not None else np.array([])
        )
        all_entity_label_mask = (
            np.array(all_entity_label_mask)
            if all_entity_label_mask is not None
            else np.array([])
        )
        entity_probs = np.array(output[1]) if output[1] is not None else np.array([])
        entity_labels = self._process_predictions(entity_preds, all_entity_label_mask)
        return entity_labels, entity_probs
