import os
import string
from typing import Any, Dict, List, Tuple, Union

import mlflow
import numpy as np
import pymorphy2
import torch
from config import Config
from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler,
                              TensorDataset)
from transformers import BertTokenizer


def add_attention_mask(
    input_ids: List[int],
    token_type_ids: List[int],
    entity_labels_ids: List[int],
    pad_token_id: int,
    mask_padding_with_zero: bool,
):
    attention_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)
    padding_length = Config.MAX_SEQ_LENGTH - len(input_ids)

    input_ids = input_ids + ([pad_token_id] * padding_length)
    attention_mask = attention_mask + (
        [0 if mask_padding_with_zero else 1] * padding_length
    )
    token_type_ids = token_type_ids + ([pad_token_id] * padding_length)
    entity_labels_ids = entity_labels_ids + ([Config.PAD_LABEL_ID] * padding_length)

    return input_ids, attention_mask, token_type_ids, entity_labels_ids


def add_special_tokens(
    tokens: List[str],
    entity_labels_ids: List[int],
    sep_token: str,
    cls_token: str,
) -> Tuple[List[str], List[int], List[int]]:
    tokens += [sep_token]
    entity_labels_ids += [Config.PAD_LABEL_ID]
    token_type_ids = [Config.SEQUENCE_A_SEGMENT_ID] * len(tokens)

    tokens = [cls_token] + tokens
    entity_labels_ids = [Config.PAD_LABEL_ID] + entity_labels_ids
    token_type_ids = [Config.CLS_TOKEN_SEGMENT_ID] + token_type_ids

    return tokens, entity_labels_ids, token_type_ids


def clean_message(text: str) -> str:
    return text.translate(str.maketrans("", "", string.punctuation)).lower()


def cut_length(
    tokens: List[str],
    entity_labels_ids: List[int],
) -> Tuple[List[str], List[int]]:
    max_len = Config.MAX_SEQ_LENGTH
    special_tokens_count = Config.SPECIAL_TOKENS_COUNT
    if len(tokens) > max_len - special_tokens_count:
        tokens = tokens[: (max_len - special_tokens_count)]
        entity_labels_ids = entity_labels_ids[: (max_len - special_tokens_count)]
    return tokens, entity_labels_ids


def get_tokens(tokenizer: BertTokenizer) -> Dict[str, Any]:
    return {
        "cls_token": tokenizer.cls_token,
        "sep_token": tokenizer.sep_token,
        "unk_token": tokenizer.unk_token,
        "pad_token_id": tokenizer.pad_token_id,
    }


def prepare_lists_for_model_input() -> Dict[str, List[List[int]]]:
    return {
        "all_input_ids": [],
        "all_attention_mask": [],
        "all_token_type_ids": [],
        "all_entity_label_mask": [],
    }


def tokenize_and_mask_sentence(
    sentence: List[str], tokenizer: BertTokenizer, tokens_dict: Dict[str, Any]
):
    tokens = []
    entity_label_mask = []
    for word in sentence:
        word_tokens = tokenizer.tokenize(word)
        if not word_tokens:
            word_tokens = [tokens_dict["unk_token"]]
        tokens.extend(word_tokens)
        entity_label_mask.extend(
            [Config.PAD_LABEL_ID + 1] + [Config.PAD_LABEL_ID] * (len(word_tokens) - 1)
        )
    return tokens, entity_label_mask


def make_dataloader(dataset: TensorDataset, stage: str = "train") -> DataLoader:
    sampler: Union[RandomSampler, SequentialSampler]
    if stage == "train":
        sampler = RandomSampler(dataset)
    else:
        sampler = SequentialSampler(dataset)

    dataloader = DataLoader(dataset, sampler=sampler, batch_size=Config.BATCH_SIZE)

    return dataloader


def accumulate_entity_predictions(
    outputs: tuple, batch: list, entity_preds: np.ndarray, entity_labels_ids: np.ndarray
) -> Tuple[np.ndarray, np.ndarray]:
    if entity_preds.size == 0:
        entity_preds = outputs[0].detach().cpu().numpy()
        entity_labels_ids = batch[3].detach().cpu().numpy()
    else:
        entity_preds = np.append(
            entity_preds, outputs[0].detach().cpu().numpy(), axis=0
        )
        entity_labels_ids = np.append(
            entity_labels_ids, batch[3].detach().cpu().numpy(), axis=0
        )
    return entity_preds, entity_labels_ids


def get_device() -> str:
    return "cuda" if torch.cuda.is_available() else "cpu"


def get_entities(path: str) -> List[str]:
    with open(os.path.join(path, Config.ENTITIES_FILE_NAME)) as f:
        entities = [entity.replace("\n", "") for entity in f.readlines()]
        return entities


def download_artifacts() -> Tuple[str, str]:
    client = mlflow.tracking.MlflowClient()
    experiment = client.get_experiment_by_name(Config.EXPERIMENT_NAME)
    experiment_id = experiment.experiment_id
    runs = client.search_runs(
        experiment_ids=[experiment_id], order_by=["start_time desc"], max_results=1
    )
    last_run_id = runs[0].info.run_id
    tokenizer_local_path = mlflow.artifacts.download_artifacts(
        run_id=last_run_id, artifact_path="tokenizer"
    )
    entities_local_path = mlflow.artifacts.download_artifacts(
        run_id=last_run_id, artifact_path="entities"
    )
    return tokenizer_local_path, entities_local_path


def preprocess_text(text: str) -> List[str]:
    return [elem for elem in text.split(".") if elem != ""]


def replace_entities(texts: List[str], entities: List[List[str]]) -> List[str]:
    replacements = {"PER": "Иван Иванович", "LOC": "Москва", "ORG": "компания"}
    morph = pymorphy2.MorphAnalyzer()
    result_texts = []

    for text, entity_list in zip(texts, entities):
        prepared_text = clean_message(text).split(" ")
        prepared_text = [word for word in prepared_text if word != ""]
        result_text = []
        i = 0

        while i < len(prepared_text):
            entity = entity_list[i]

            if entity == "O":
                result_text.append(prepared_text[i])
                i += 1
            else:
                entity_type = entity.split("_")[1]

                entity_words = []
                while i < len(prepared_text) and (
                    entity_list[i].startswith("I_")
                    or entity_list[i] == f"B_{entity_type}"
                ):
                    entity_words.append(prepared_text[i])
                    i += 1

                replacement = replacements.get(entity_type, entity_words[0])

                for word in entity_words:
                    parsed_word = morph.parse(word)[0]
                    replacement_word = morph.parse(replacement)[0]
                    inflected_word = replacement_word.inflect(parsed_word.tag.grammemes)
                    if inflected_word:
                        result_text.append(inflected_word.word)
                    else:
                        result_text.append(replacement_word.word)

        result_texts.append(" ".join(result_text))

    return result_texts
