PYTHON_INTERPRETER = python
PYTHON_VERSION = 3.10
VENV = .venv
FOLDER = app/


set_config:
	@echo "setting the config and creating poetry.lock ..."
	poetry config virtualenvs.in-project true
	poetry lock --no-update


.PHONY: install
install:
	@echo "installing dependencies..."
	poetry install --no-root


.PHONY: activate
activate:
	@echo "virtual env activation $$(poetry env info --path)/bin/activate"
	source $$(poetry env info --path)/bin/activate


.PHONY: setup
setup: set_config install activate


.PHONY: clean
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete
	find . -type d -name ".idea" -delete
	rm -rf .mypy_cache
	rm -rf .ruff_cash
	rm -rf $(VENV)
	rm -r .idea


.PHONY: lint
lint:
	poetry run ruff check $(FOLDER)
	poetry run mypy $(FOLDER)


.PHONY: format
format:
	poetry run ruff format $(FOLDER)
	poetry run isort $(FOLDER)


.PHONY: sync_data_down
sync_data_down:
	aws s3 sync s3://-/data/\
		data/  --profile -



.PHONY: sync_data_up
sync_data_up:
	aws s3 sync s3://-/data/ data/\
		 --profile $(PROFILE)