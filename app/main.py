import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI
from Predict import Predictor
from prometheus_fastapi_instrumentator import Instrumentator
from utils import preprocess_text, replace_entities

load_dotenv()

app = FastAPI()
Instrumentator().instrument(app).expose(app)

predictor = Predictor()


@app.post("/predict")
async def predict(doc_text: str):
    processed_text = preprocess_text(doc_text)
    entity_labels, _ = predictor.predict(sentences=processed_text)
    processed_text = replace_entities(processed_text, entity_labels)
    return {"processed_text": processed_text}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8383)
